import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: []
})
export class PromesasComponent {

  constructor() {


    this.contarTres()
      .then(
        mensaje => console.log('Terminó', mensaje)
      )
      .catch(err =>
        console.log('Error al resolver la promesa', err));
  }

  contarTres(): Promise<boolean> {

   return new Promise((resolve, reject) => {
      let contador = 0;
      const intervalo = setInterval(() => {
        contador += 1;
        console.log(contador);
        if (contador === 3) {
          //reject('simplemente un error');
          resolve(true);
          clearInterval(intervalo)
        }
      }, 1000);
    });
  }
}

