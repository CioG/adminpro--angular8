import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  // creo objeto de tipo ajustes. Han de coincidir con los definidos en interface
  ajustes: Ajustes = {
    temaUrl: 'assets/css/colors/default.css',
    tema: 'default'
  };


  constructor(@Inject(DOCUMENT) private _document) {
this.cargarAjustes();

   }

  guardarAjustes() {
    // console.log("Guardado en localStorage");

    // localStorge solo graba datos en formato string, para pasar un objeto a un string , utilizo 'stringify'
    localStorage.setItem('ajustes', JSON.stringify(this.ajustes));
  }

  cargarAjustes() {
    if (localStorage.getItem('ajustes')) {

      //reconvierto (paso de string(JSON) a objeto. Para ello utilizo 'parse')
      this.ajustes = JSON.parse(localStorage.getItem('ajustes'))
      // console.log("cargando del localStorage los ajustes");

      this.aplicarTema(this.ajustes.tema)

    } else {
      // console.log("Usando valores por defecto, pues no tenemos data todavia");

    }
  }
aplicarTema(tema: string) {
  let url = `assets/css/colors/${tema}.css`;
  this._document.getElementById('tema').setAttribute('href', url);
  this.ajustes.tema = tema;
  this.ajustes.temaUrl = url;
  this.guardarAjustes();
}

}
//1) defino interface, con ello restrinjo la forma de definir los ajustes
interface Ajustes {
  tema: string;
  temaUrl: string;
}
