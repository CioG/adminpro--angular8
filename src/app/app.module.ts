import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
//Modules
import { PagesModule } from './pages/pages.module';
//Servicios
import { ServiceModule } from './services/service.module';



//import { GraficoDonaComponent } from './components/grafico-dona/grafico-dona.component'; //TEMPORAL

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    //  GraficoDonaComponent,

  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    PagesModule,
    FormsModule,
    ServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
