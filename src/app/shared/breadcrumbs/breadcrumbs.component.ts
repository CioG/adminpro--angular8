import { Component } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Title, Meta, MetaDefinition } from '@angular/platform-browser';
@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styles: []
})
export class BreadcrumbsComponent {

  titulo: string;
  constructor(private r: Router, private title: Title, private meta: Meta) {

    this.getDataRoute()

      .subscribe(data => {
        console.log(data);
        this.titulo = data.titulo;
        //Para cabiar el titulo al la ventana del navegadot
        this.title.setTitle(this.titulo);
        //para metar los metatags de busqueda para google, por ejemplo.
        const metaTag: MetaDefinition = {
          name: 'description',
          content: this.titulo
        };
        this.meta.updateTag(metaTag)
      })
  }

  getDataRoute() {
    return this.r.events.pipe(

      filter(evento => evento instanceof ActivationEnd),
      filter((evento: ActivationEnd) => evento.snapshot.firstChild === null),
      map((evento: ActivationEnd) => evento.snapshot.data)
    )
  }

}
