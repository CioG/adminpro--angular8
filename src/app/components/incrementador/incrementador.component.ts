import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styleUrls: ['./incrementador.component.scss']
})
export class IncrementadorComponent implements OnInit {

  @ViewChild('txtProgress',{static: false}) txtProgress: ElementRef;

  @Input() leyenda: string = 'Leyenda';
  @Input() progreso: number = 50;

  // Esta es la sintaxis para poder emitir un número como un evento
  @Output('actualizaValor') cambioValor: EventEmitter<number> = new EventEmitter();
  constructor() {
    console.log('Leyenda', this.leyenda);
    // console.log('Progreso', this.progreso);

  }

  ngOnInit() {
  }

  onChanges(newValue: number) {

    console.log(this.txtProgress);

    //let elemHTML: any = document.getElementsByName('progreso')[0];
    //console.log(elemHTML.value);

    console.log(newValue);
    this.cambioValor.emit(this.progreso);
    if (newValue >= 100) {
      this.progreso = 100;
    } else if (newValue <= 0) {
      this.progreso = 0;
    } else {
      this.progreso = newValue;
    }

    //elemHTML.value = this.progreso;
    this.txtProgress.nativeElement.value = this.progreso;
    this.cambioValor.emit(this.progreso);
    this.txtProgress.nativeElement.focus();
  }



  cambiarValor(valor: number) {

    if (this.progreso >= 100 && valor > 0) {
      this.progreso = 100;
      return;
    }
    if (this.progreso <= 0 && valor < 0) {
      this.progreso = 0;
      return;
    }
    this.progreso = this.progreso + valor;

    this.cambioValor.emit(this.progreso);

    // console.log(this.progreso);
  }

}
