import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare function init_plugins();

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private r: Router) { }

  ngOnInit() {
    init_plugins();
  }

  Ingresar() {
    console.log("Ingresando...");
    this.r.navigate(['/dashboard']);
    
  }

}
